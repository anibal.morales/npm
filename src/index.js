const messages = [
    "Oscar",
    "Anibal",
    "Diego",
    "Mariela",
    "Pedro",
    "Mayra",
    "Mynor",
    "Carmen",
    "Cristina",
    "Lucia",
    "Marleny"
];

const randomMsg = () => {
    const message = messages[Math.floor(Math.random() * messages.length)];
    console.log(message);
}

module.exports = { randomMsg };
